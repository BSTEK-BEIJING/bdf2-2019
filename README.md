# bdf2-2019

#### 介绍
基于bdf2-core-2.1.0.jar， dorado-core-7.6.0.2.jar，spring 4.0整合的开发平台。

#### 软件架构
软件架构说明


#### 安装教程

1.  从远程仓库导入工程到eclipse中。
2.  启动服务。

#### 使用说明

1.  http://localhost:8080/bdf2-2019/login.jsp
2.  管理员用户名和密码：admin/123456

#### 部分页面截图

![test](/web/img/login.jpg)

![test](/web/img/main2.jpg)

![test](/web/img/user.jpg)

![test](/web/img/url.jpg)

![test](/web/img/icon.jpg)

![test](/web/img/roleMember.jpg)

![test](/web/img/component.jpg)

![test](/web/img/roleUrl.jpg)

![test](/web/img/dbConsole.jpg)

![test](/web/img/job.jpg)

![test](/web/img/job2.jpg)

