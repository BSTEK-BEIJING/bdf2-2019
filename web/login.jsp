<%@page import="com.bstek.dorado.core.Configure"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Welcome</title>
<%
String loginPage=request.getContextPath()+Configure.getString("bdf2.formLoginUrl");
%>
<script type="text/javascript">
	function load(){
		window.location.href="<%=loginPage%>";
	}
</script>
</head>
<body onload="load();">

</body>
</html>